from collections import Counter

import logging

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
log = logging.getLogger(__name__)


def format_result(json_data):
    log.info("build result start")
    occupations = []
    skills = []
    traits = []
    locations = []

    for d in json_data:
        occupations.extend(d['occupation'])
        skills.extend(d['skill'])
        traits.extend(d['trait'])
        locations.extend(d['location'])

    sorted_result = {
        'occupation_data': Counter(occupations).most_common(),
        'skill_data': Counter(skills).most_common(),
        'trait_data': Counter(traits).most_common(),
        'location_data': Counter(locations).most_common()
    }
    log.info("build result end")
    return sorted_result

from typing import Generator
import time
import logging
import certifi
from ssl import create_default_context
from elasticsearch import Elasticsearch, ElasticsearchException
from elasticsearch.helpers import scan

import settings

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def load_data() -> Generator:
    client = create_elastic_client_with_retry()
    query = {"query": {"bool": {"filter": [{"term": {"removed": False}}]}}}
    logger.info("load ads generator start")
    scan_result = scan(client, query, index=settings.ES_INDEX)
    logger.info("load ads generator after scan")
    yield '['
    for ad in scan_result:
        yield ad['_source']["keywords"]['enriched']
        yield ','
    yield ']'
    logger.info("load ads generator end")


def refresh_from_elastic():
    logger.info('refresh from elastic start')
    generator = load_data()
    raw_data = unpack(generator)
    logger.info('refresh from Elastic done')
    return raw_data


def unpack(generator):
    logger.info('unpack')
    json_data = []
    for g in generator:
        if isinstance(g, dict):
            json_data.append(g)
    logger.info('unpack end')
    return json_data


def create_elastic_client_with_retry():
    host = settings.ES_HOST
    port = settings.ES_PORT
    user = settings.ES_USER
    password = settings.ES_PWD
    es_timeout = settings.ES_DEFAULT_TIMEOUT
    start_time = int(time.time() * 1000)
    logger.info(f"Creating Elastic client, host: {host}, port: {port} user: {user} ")
    fail_count = 0
    max_retries = settings.ES_RETRIES

    for i in range(max_retries):
        try:
            if user and password:
                elastic = Elasticsearch([host],
                                        port=port,
                                        use_ssl=True,
                                        scheme='https',
                                        ssl_context=create_default_context(cafile=certifi.where()),
                                        http_auth=(user, password),
                                        timeout=es_timeout)
            else:
                elastic = Elasticsearch([{'host': host, 'port': port}])
            # check connection
            info = elastic.info()
            logger.info(f"Successfully connected to Elasticsearch node at {settings.ES_HOST}:{settings.ES_PORT}")
            logger.debug(f"Elastic info: {info}")
            logger.debug(
                f"Elasticsearch node created and connected after: {int(time.time() * 1000 - start_time)} milliseconds")
            return elastic

        except (ElasticsearchException, Exception) as e:
            fail_count += 1
            time.sleep(settings.ES_RETRIES_SLEEP)
            logger.warning(f"Failed to connect to Elastic, failed attempt: {fail_count}. Error: {e}")
            if fail_count >= max_retries:
                logger.error(f"Failed to connect to Elastic after attempts: {fail_count}. Exit!")
                return None

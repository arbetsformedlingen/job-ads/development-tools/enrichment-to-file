import json
from datetime import datetime

import settings


def file_name():
    return f"enrichment_{datetime.now().strftime(settings.FILE_NAME_DATE_FORMAT)}.json"


def write_json(data):
    with open(file_name(), 'w', encoding='utf8') as f:
        json.dump(data, f, ensure_ascii=False)

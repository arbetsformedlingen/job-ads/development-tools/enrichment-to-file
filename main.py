from src.loader import refresh_from_elastic
from src.write_file import write_json
from src.formatter import format_result

if __name__ == '__main__':
    raw_data = refresh_from_elastic()
    formatted_data = format_result(raw_data)
    write_json(formatted_data)

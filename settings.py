import os

# Elasticsearch settings
ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")
ES_INDEX = os.getenv("ES_INDEX", "platsannons-read")

ES_RETRIES = int(os.getenv("ES_RETRIES", 1))
ES_RETRIES_SLEEP = int(os.getenv("ES_RETRIES_SLEEP", 2))

ES_DEFAULT_TIMEOUT = int(os.getenv("ES_DEFAULT_TIMEOUT", 30))

FILE_NAME_DATE_FORMAT = "%Y-%m-%d"

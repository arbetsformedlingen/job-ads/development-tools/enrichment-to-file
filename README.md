A program that loads enrichment data from a jobads index in Elastic, calculates how many occurences there are of each word and sorts the data by number.  
The result is written to a json file with today's date as part of the file name

### Usage: 
- set environment variables used by Elastic (settings.py)  
- run `python main.py`

### Todo:
- Upload file (zipped) to data.jobtechdev.se
- Dockerize
